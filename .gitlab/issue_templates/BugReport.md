**Summary:**

(Summarize the bug encountered concisely. Try to use also a
descriptive title for the issue)

**Code version:**

(The Siesta version in which the problem was found. We can't assume
that you have the very latest version, and a problem that exists in
one version may not exist in another. Use the version number printed
at the top of any output file (also found in file version.info in the
top directory).

**System information:**

(The platform on which the problem was found, and the operating system
 and compiler version. State whether the problem appears in a serial
 and/or parallel run. Including your arch.make is also a good idea.
 You might want to check whether the problem is due to a compiler or library
 error, by using a different platform or environment.)

**Steps to reproduce:**

(How one can reproduce the issue - this is very important.  If you can
show the faulty behavior already in a small test, that could be
preferable to dealing with it in a production case that might take too
long to reproduce.)

**Example Project:**

(If possible, please create an example project here on GitLab.com that
exhibits the problematic behaviour, and link to it here in the bug
report. For example, you could put in the project fdf and
pseudopotential files, and output files)

**What is the current bug behavior?**

(What actually happens)

**What is the expected correct behavior?**

(What you would expect to see instead)

**Relevant logs and/or screenshots**

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise. For large files, consider
creating a Gitlab project, as discussed above.)

**Possible fixes**

(If you have any idea about how to fix the problem, by all means tell
us!. If you can, link to the line of code that might be responsible
for the problem)

/label ~bug_report
