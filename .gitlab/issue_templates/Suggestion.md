**Summary:**

(Summarize the topic of your suggestion. Try to use also a
descriptive title for the issue)

**Description:**

(Longer description)

**Supporting material:**

(You might have already some material or even preliminary code
that serves to document the suggestion. Ideally, it would be in
a Gitlab project, but you could attach other (not too large) files)

/label ~suggestion
